﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EditCamera : MonoBehaviour {
	// sensitivity values

	[SerializeField] private float mouseSpeed;
	[SerializeField] private float keyMovementSpeed;
	[SerializeField] private float shiftModifiedMovementSpeed;
	[SerializeField] private float mouseWheelSpeed;

	// smoothing values
	[SerializeField] private float distanceSmoothing;
	[SerializeField] private float orbitSmoothing;
	[SerializeField] private float moveSpeedSmoothing;
	[SerializeField] private float mouseFpsSmoothing;

	// distance limits
	[SerializeField] private float minOrbitDistance;
	[SerializeField] private float maxOrbitDistance;

	private Vector3 targetPoint;

	// Record starting values so we can reset easily
	[SerializeField] private float defaultAngleX;
	[SerializeField] private float defaultAngleY;
	[SerializeField] private float defaultOrbitDistance;

	// orbit values (serialized to set start values in inspector)
	private float angleX;
	private float angleY;
	private float orbitDistance;

	// fps values
	private bool fpsMode;
	private float moveSpeedX;
	private float moveSpeedY;
	private float moveSpeedZ;

	private float targetAngleX;
	private float targetAngleY;
	private float targetMoveSpeedX;
	private float targetMoveSpeedY;
	private float targetMoveSpeedZ;
	private float targetOrbitDistance;


	public float MouseSensitivity {
		set { mouseSpeed = value; }
		get { return mouseSpeed; }
	}

	private void Start() {
		targetAngleX = angleX = defaultAngleX;
		targetAngleY = angleY = defaultAngleY;
		targetOrbitDistance = orbitDistance = defaultOrbitDistance;
	}

	private void Update() {
		float mouseX = Input.GetAxis("Mouse X") * mouseSpeed;
		float mouseY = Input.GetAxis("Mouse Y") * mouseSpeed;

		if (Input.GetKeyDown(KeyCode.F)) {
			fpsMode = false;
			targetPoint = Vector3.zero;
			targetAngleX = defaultAngleX;
			targetAngleY = defaultAngleY;
			targetOrbitDistance = defaultOrbitDistance;
		}

		if (Input.GetKey(KeyCode.LeftAlt)) {
			if (Input.GetMouseButton(0)) {
				fpsMode = false;
				targetAngleX += mouseX;
				targetAngleY -= mouseY;
			} else if (Input.GetMouseButton(1)) {
				fpsMode = false;
				targetOrbitDistance = Mathf.Clamp(targetOrbitDistance + mouseY, minOrbitDistance, maxOrbitDistance);
			} else if (Input.GetMouseButton(2)) {
				fpsMode = true;
				targetMoveSpeedX = -mouseX * keyMovementSpeed;
				targetMoveSpeedZ = -mouseY * keyMovementSpeed;
			} else {
				targetMoveSpeedX = 0f;
				targetMoveSpeedY = 0f;
				targetMoveSpeedZ = 0f;
			}
		} else {
			if (Input.GetMouseButton(1)) {
				fpsMode = true;
				targetAngleX += mouseX;
				targetAngleY -= mouseY;

				float moveSpeed = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				                  	? shiftModifiedMovementSpeed
				                  	: keyMovementSpeed;
				targetMoveSpeedX = Input.GetAxis("Horizontal") * moveSpeed;
				targetMoveSpeedY = Input.GetAxis("Vertical") * moveSpeed;
			} else {
				targetMoveSpeedX = 0f;
				targetMoveSpeedY = 0f;
				targetMoveSpeedZ = 0f;
			}
		}

		WrapAngle(ref targetAngleX);
		WrapAngle(ref targetAngleY);

		// mouse wheel always, 
		//if (!EventSystem.current.IsPointerOverGameObject()) {
			targetOrbitDistance += zoomAdd;
		//}
		// smooth values
		orbitDistance = Mathf.Lerp(orbitDistance, targetOrbitDistance, distanceSmoothing * Time.deltaTime);
		angleX = Mathf.LerpAngle(angleX, targetAngleX,
		                         (fpsMode ? mouseFpsSmoothing : orbitSmoothing) * Time.deltaTime);
		angleY = Mathf.LerpAngle(angleY, targetAngleY,
		                         (fpsMode ? mouseFpsSmoothing : orbitSmoothing) * Time.deltaTime);
		moveSpeedX = Mathf.Lerp(moveSpeedX, targetMoveSpeedX, moveSpeedSmoothing * Time.deltaTime);
		moveSpeedY = Mathf.Lerp(moveSpeedY, targetMoveSpeedY, moveSpeedSmoothing * Time.deltaTime);
		moveSpeedZ = Mathf.Lerp(moveSpeedZ, targetMoveSpeedZ, moveSpeedSmoothing * Time.deltaTime);

		// perform the movements
		if (fpsMode) {
			transform.rotation = Quaternion.Euler(angleY, angleX, 0);
			transform.position += (transform.right * moveSpeedX + transform.forward * moveSpeedY + transform.up * moveSpeedZ) * Time.deltaTime;
			targetPoint = transform.position + transform.forward * orbitDistance;
		} else {
			Quaternion rotation = Quaternion.Euler(angleY, angleX, 0);
			Vector3 position = rotation * Vector3.forward * -orbitDistance + targetPoint;
			transform.position = position;
			transform.rotation = rotation;
		}
	}

	private float zoomAdd {
		get {
			float scrollAxis = Input.GetAxis("Mouse ScrollWheel");
			return -mouseWheelSpeed * scrollAxis;
		}
	}

	private void WrapAngle(ref float angle) {
		if (angle < -360f) angle += 360f;
		if (angle > 360f) angle -= 360F;
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(targetPoint, 1f);
	}
}