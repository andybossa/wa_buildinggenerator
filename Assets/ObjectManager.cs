﻿using UnityEngine;
[ExecuteInEditMode]
public class ObjectManager : MonoBehaviour {

	#region SINGLETON

	private static ObjectManager _instance;

	public static ObjectManager Instance {
		get { return _instance; }
	}

	public void OnApplicationQuit() {
		_instance = null;
	}

	#endregion

	public GameObject[] assets;
	
	public void Awake() {
		if (ObjectManager.Instance != null) {
			DestroyImmediate(gameObject);
			return;
		}
		_instance = this;
	}
	
	public GameObject GetAssetFromName(string name) {
		for (int i = 0; i < assets.Length; i++) {
			if (assets[i].name == name) return assets[i];
			
		}
		return null;
	}
}