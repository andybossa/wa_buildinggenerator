﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class BuildingMakerUI : MonoBehaviour 
{
	
	public static BuildingMakerUI Instance { get; private set; }
	public InputField BuildingNameField;
	[SerializeField] private Button _saveBtn;
	[SerializeField] private Button _loadBtn;
	
	[HeaderAttribute("Toolbox")]
	[SerializeField] private Toggle _selectTool;
	[SerializeField] private Toggle _clearTool;
	[SerializeField] private Toggle _brushTool;
	
	[SerializeField] private GameObject _pallet;
	
	[HeaderAttribute("Level Loading")]
	[SerializeField] private GameObject _loadLevelDialogue;
	[SerializeField] private Button _loadLevelDialougeButton;
	[SerializeField] private Transform _loadBuildingListContent;
	[SerializeField] private Vector2 _buttonOffset;
	[SerializeField] private GameObject _levelSelectButton;
	
	[HeaderAttribute("BuildingNavigation")]
	[SerializeField] private Text _floorLabel;
	
	private BuildingMaker bm;
	private string _rootPath;
	private ScrollRect _loadBuildingList; 
	private ToggleGroup _levelLoadingToggleGroup;
	private string _fileToLoad;
	private bool _lastDirty;
	
	void Awake () 
	{	
		Instance = this;
		_loadBuildingList = _loadLevelDialogue.GetComponentInChildren<ScrollRect>();
		_rootPath = Application.dataPath;
	}
	
	void Start ()
	{
		bm = BuildingMaker.Instance;
		ShowLoadBuildingUI(false);
		_levelLoadingToggleGroup = _loadBuildingListContent.GetComponent<ToggleGroup>();
		_floorLabel.text = "FLOOR: ALL";
		_pallet.SetActive(true);
	}
	
	void Update ()
	{
		_saveBtn.interactable = bm.Dirty;
		
		if (_loadLevelDialogue.activeSelf)
			_loadLevelDialougeButton.enabled = _fileToLoad == "" ? false : true;
	}	
	
	public void ShowLoadBuildingUI (bool IsVisible)
	{
		_loadBtn.interactable = !IsVisible;
		if(bm.Dirty && IsVisible)
		{
			_lastDirty = bm.Dirty;
			bm.Dirty = false;
		}
		else if (_lastDirty && !IsVisible && !bm.Dirty)
		{
			_lastDirty = bm.Dirty;
			bm.Dirty = true;
		}
		
		_loadBuildingList.transform.parent.gameObject.SetActive(IsVisible);
		
		if(IsVisible)
		{
			GetLevelList();
		}
		else
		{
			ClearLevelList();
		}
			
	}
	
	public void DoLoadLevel ()
	{
		bm.OnLoad(_fileToLoad);
		BuildingNameField.text = Path.GetFileNameWithoutExtension(_fileToLoad);
		ShowLoadBuildingUI(false);
	}
	
	public void DoClearLevel ()
	{
		BuildingNameField.text = "";
		bm.OnClear();
	}
	
	public void SelectToolActive() 
	{
		bm.CurrentTool = BuildingTool.Select;
		_pallet.SetActive(true);
	}
	public void BrushToolActive()
	{
		bm.CurrentTool = BuildingTool.Brush;
		_pallet.SetActive(true);
	}
	public void ClearToolActive() 
	{
		bm.CurrentTool = BuildingTool.Clear;
		_pallet.SetActive(false);
	}
	
	public void DoFloorUp()
	{
		bm.SetFloor(bm.CurrentFloor + 1);
		_floorLabel.text = ("FLOOR: " + (bm.CurrentFloor).ToString());
	}
	
	public void DoFloorDown()
	{
		bm.SetFloor(bm.CurrentFloor - 1);
		_floorLabel.text = bm.CurrentFloor == -1 ? "FLOOR: ALL" : ("FLOOR: " + (bm.CurrentFloor).ToString());
	}
	
	public void DoFloorAll()
	{
		bm.SetFloor();
		_floorLabel.text = "FLOOR: ALL";
	}
	
	void GetLevelList ()
	{
		string[] levelList = Directory.GetFiles(_rootPath + bm.BuildingGeneratorPath, "*.json");
		int n = 0;
		foreach(string lvl in levelList)
		{
			var btn = GameObject.Instantiate(_levelSelectButton,Vector3.zero, Quaternion.identity) as GameObject;
			btn.transform.SetParent(_loadBuildingListContent, false);
			btn.GetComponent<RectTransform>().anchoredPosition =_buttonOffset * n;
			btn.GetComponentInChildren<Text>().text = Path.GetFileNameWithoutExtension(lvl);
			var tgl = btn.GetComponent<Toggle>();
			tgl.group = _levelLoadingToggleGroup;
			n += 1;
		}
	}
	
	public void UpdateLevelLoadSelected ()
	{
		foreach(var tgl in _levelLoadingToggleGroup.ActiveToggles())
		{
				if(tgl.isOn)
					_fileToLoad = tgl.GetComponentInChildren<Text>().text;
		}
	}
	
	public void ClearLevelList ()
	{
		for(int i = 0; i < _loadBuildingListContent.childCount; i++)
		{
			Destroy(_loadBuildingListContent.GetChild(i).gameObject);
		}
	}
}
