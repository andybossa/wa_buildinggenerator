﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
public sealed class ExposedMethodAttribute : PropertyAttribute {
	public string methodName;
	
	public ExposedMethodAttribute(string methodName) {
		this.methodName = methodName;
	}

}