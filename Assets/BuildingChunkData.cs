public class ExteriorWall
{
    public string fbxName { get; set; }
    public int id { get; set; }
}

public class InteriorWallFront
{
    public string fbxName { get; set; }
    public int id { get; set; }
}

public class InteriorWallSide
{
    public string fbxName { get; set; }
    public int id { get; set; }
}

public class InteriorTwinDoorway
{
    public string fbxName { get; set; }
    public int id { get; set; }
}

public class SaborianAppartment
{
    public ExteriorWall[] ExteriorWall { get; set; }
    public InteriorWallFront[] InteriorWallFront { get; set; }
    public InteriorWallSide[] InteriorWallSide { get; set; }
    public InteriorTwinDoorway[] InteriorTwinDoorway { get; set; }
}

public class BuildingChunkData
{
    public SaborianAppartment SaborianAppartment { get; set; }
}