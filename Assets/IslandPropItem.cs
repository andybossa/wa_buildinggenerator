using System.Collections.Generic;

public class IslandPropItem {
	public string name;
	public List<IslandPropItem> folders;
	public List<string> files;

	public void AddItem(string path) {
		if (path.IndexOf('/') != -1) {
			string[] splitPath = path.Split(new[] {'/'}, 2);
			GetChildFolder(splitPath[0]).AddItem(splitPath[1]);
		} else {
			if (files == null) files = new List<string>();
			files.Add(path);
		}
	}

	private IslandPropItem GetChildFolder(string child) {
		if (folders == null) folders = new List<IslandPropItem>();
		for (int i = 0; i < folders.Count; i++) {
			if (folders[i].name == child) return folders[i];
		}
		IslandPropItem newChild = new IslandPropItem {name = child, folders = new List<IslandPropItem>()};
		folders.Add(newChild);
		return newChild;
	}

	public void GetAllFiles(string prefix, List<string> fullList, bool addPrefix) {
		if (files != null) {
			foreach (string file in files) {
				fullList.Add((addPrefix) ? prefix + file : file);
			}
		}

		if (folders != null) {
			foreach (IslandPropItem folder in folders) {
				folder.GetAllFiles((addPrefix) ? prefix + folder.name + "/" : "", fullList, addPrefix);
			}
		}
	}
}
