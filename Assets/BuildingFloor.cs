﻿using UnityEngine;

public class BuildingFloor : MonoBehaviour {

	public int Floor = -1;
	
	void Awake ()
	{
		string nameSplit = this.gameObject.name.Split(new System.Char[] {'_'} )[1];
		int.TryParse(nameSplit, out Floor);
	}
}
