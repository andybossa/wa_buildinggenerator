﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ExposedMethodAttribute))]
public class ExposedMethodDrawer : PropertyDrawer {

	ExposedMethodAttribute MethodAttribute { get { return ((ExposedMethodAttribute)attribute); } }
	private MethodInfo method;
	private Dictionary<string, object> parameters;
	private bool methodError = false;
	private int parameterCount;
	private const float parameterGUIHeight = 18f;

	public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label) {
		if (methodError) return;
		if (method == null) FindMethod(prop);
		if (method == null) return;
		
		Rect currentPos = pos;
		currentPos.x += 8f;
		currentPos.y += 2f;
		GUI.Label(currentPos, prop.stringValue, EditorStyles.boldLabel);
		currentPos.x -= 8f;
		currentPos.y -= 2f;
		
		currentPos.height = 46 + method.GetParameters().Length*parameterGUIHeight;
		currentPos.width -= 8f;
		GUI.Box(currentPos, "", EditorStyles.helpBox);
//		GUI.color = EditorGUIUtility.isProSkin ? new Color(1, 1, 1, 0.1f) : new Color(0, 0, 0, 0.1f);
//		GUI.DrawTexture(currentPos, EditorGUIUtility.whiteTexture);
//		GUI.color = Color.white;
		
		currentPos.height = 16f;
		currentPos.y += 22;
		currentPos.width -= 8f;
		
		EditorGUI.indentLevel = 1;

		if (method.GetParameters().Length > 0) {
			
			// draw the params
			foreach (var param in method.GetParameters()) {

				var paramKey = param.Name;

				if (!parameters.ContainsKey(paramKey))
					parameters.Add(paramKey,
								   param.ParameterType.IsValueType
									? System.Activator.CreateInstance(param.ParameterType)
									: null);


				// add any supported types that you want here
				if (param.ParameterType == typeof(int)) parameters[paramKey] = EditorGUI.IntField(currentPos, paramKey, (int)parameters[paramKey]);
				else if (param.ParameterType == typeof(string)) parameters[paramKey] = EditorGUI.TextField(currentPos, paramKey, (string)parameters[paramKey]);
				else if (param.ParameterType == typeof(bool)) parameters[paramKey] = EditorGUI.Toggle(currentPos, paramKey, (bool)parameters[paramKey]);
				else if (param.ParameterType == typeof(float)) parameters[paramKey] = EditorGUI.FloatField(currentPos, paramKey, (float)parameters[paramKey]);
				else if (param.ParameterType == typeof(Vector2)) parameters[paramKey] = EditorGUI.Vector2Field(currentPos, paramKey, (Vector2)parameters[paramKey]);
				else if (param.ParameterType == typeof(Vector3)) parameters[paramKey] = EditorGUI.Vector3Field(currentPos, paramKey, (Vector3)parameters[paramKey]);
				else if (param.ParameterType == typeof(Color)) parameters[paramKey] = EditorGUI.ColorField(currentPos, paramKey, (Color)parameters[paramKey]);
				else if (param.ParameterType.BaseType == typeof(System.Enum)) parameters[paramKey] = EditorGUI.EnumPopup(currentPos, paramKey, (Enum)parameters[paramKey]);
				else if (param.ParameterType == typeof(Transform)) parameters[paramKey] = EditorGUI.ObjectField(currentPos, paramKey, (Transform)parameters[paramKey], typeof(Transform), true);
				else Debug.LogWarning("Unsupported parameter type in method '"+ method.Name + "'");
				//parameters[paramKey] = EditorGUI.EnumPopup(currentPos, paramKey, (Enum)parameters[paramKey]);
				currentPos.y += parameterGUIHeight;
			}
		}

		currentPos.y += 4f;
		currentPos.x += 8f;
		currentPos.width -= 8f;
		if (GUI.Button(currentPos, "Invoke Method")) {
			var values = new object[parameters.Count];
			parameters.Values.CopyTo(values, 0);
			method.Invoke(prop.serializedObject.targetObject, values);
		}
	}

	// Here you must define the height of your property drawer. Called by Unity.
	public override float GetPropertyHeight(SerializedProperty prop, GUIContent label) {
		if (parameters == null) FindMethod(prop);
		return 50 + parameterCount * parameterGUIHeight;
	}

	private void FindMethod(SerializedProperty prop) {
		Type type = prop.serializedObject.targetObject.GetType();
		method = type.GetMethod(MethodAttribute.methodName, BindingFlags.Public | BindingFlags.Instance);
		// maybe its private
		if (method == null) method = type.GetMethod(MethodAttribute.methodName, BindingFlags.NonPublic | BindingFlags.Instance);
		// Maybe typo'd or is not instance method
		if (method == null) {
			Debug.LogWarning("Couldn't find method '" + MethodAttribute.methodName + "'. Check spelling and make sure it isn't static");
			methodError = true;
			return;
		}
		parameterCount = method.GetParameters().Length;
		parameters = new Dictionary<string, object>();
	}
}