using UnityEngine;
using System.Collections;

public class EditorCameraControllerFPS: MonoBehaviour {

	[SerializeField] private float rotationSensitivity = 3f;
	[SerializeField] private float yMinLimit = -89f;
	[SerializeField] private float yMaxLimit = 89f;
	
	[SerializeField] private float defaultMovementSpeed = 1f;
	[SerializeField] private float modifiedMovementSpeed = 10f;

	private Transform t;
	private float x, y;

	void Awake () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
		t = transform;
	}

	public void LateUpdate() {
		if(Input.GetMouseButton(1))
		{
			Cursor.lockState = CursorLockMode.Locked;

			x += Input.GetAxis("Mouse X") * rotationSensitivity;
			y = ClampAngle(y - Input.GetAxis("Mouse Y") * rotationSensitivity, yMinLimit, yMaxLimit);

			// Rotation
			t.rotation = Quaternion.AngleAxis(x, Vector3.up) * Quaternion.AngleAxis(y, Vector3.right);
			
			// Movement
			float xMovement = Input.GetAxis("Horizontal");
			float yMovement = (Input.GetKey(KeyCode.Space) ? 1f : 0f) + (Input.GetKey(KeyCode.LeftControl) ? -1f : 0f);
			float zMovement = Input.GetAxis("Vertical");
			float movementSpeed = Input.GetKey(KeyCode.LeftShift) ? modifiedMovementSpeed : defaultMovementSpeed;
			t.Translate(new Vector3(xMovement, yMovement, zMovement) * movementSpeed);
		} 
		else
		{
			Cursor.lockState = CursorLockMode.None;
		}
	}
	

	// Clamping Euler angles
	private float ClampAngle (float angle, float min, float max) {
		if (angle < -360) angle += 360;
		if (angle > 360) angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}

}