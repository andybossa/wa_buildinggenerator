﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public enum ChunkType
{
	ExteriorWall,
	InteriorWallFront,
	InteriorWallSide,
	InteriorTwinDoorway,
	InteriorFloor,
	BuildingFront,
	BuildingRear,
}

public class BuildingChunk : MonoBehaviour {
	public string ChunkType;
	private BoxCollider _col;
	private BuildingMaker _bm;
	public int ChunkId
	{
		get; set;
	}
	public GameObject CurrentChunk 
	{
		get; set;
	}
	
	void Start ()
	{
		_bm = GameObject.FindObjectOfType<BuildingMaker>();
		_col = gameObject.AddComponent<BoxCollider>();
		_col.isTrigger = true;
		ChunkId = -1;
	}
	
	void OnMouseOver ()
	{
		if(Input.GetMouseButton(0))
			_bm.ChunkClicked(this);
	}
}
