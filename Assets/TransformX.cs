﻿using UnityEngine;

public static class TransformX {

	public static void SetLayerRecursively(this Transform obj, int aLayer) {
		foreach (Transform T in obj.GetComponentsInChildren<Transform>()) {
			T.gameObject.layer = aLayer;

		}
	}

	public static void Zero(this Transform obj, bool local = true) {
		if (local) {
			obj.localPosition = Vector3.zero;
			obj.localRotation = Quaternion.Euler(0,0,0);
		} else {
			obj.position = Vector3.zero;
			obj.rotation = Quaternion.Euler(0,0,0);
		}
	}

	public static void MatchTransform(this Transform obj, Transform target) {
		obj.position = target.position;
		obj.rotation = target.rotation;
	}

	public static string GetFullPath(this Transform obj, bool ignoreRoot = false) {
		Transform t = obj;
		string fullPath = "";
		while (t != null) {
			fullPath = "/" + t.name + fullPath;
			t = t.parent;
		}
		fullPath = fullPath.Substring(1);
		if (ignoreRoot) fullPath = fullPath.Substring(fullPath.IndexOf('/') + 1);
		return fullPath;
	}
}