﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
[ExecuteInEditMode]
public class GenerateBuildingChunkData : MonoBehaviour {
	
	[ExposedMethodAttribute("GenerateChunkData")] public string generateChunkData = "Generate Data";
		
	private BuildingMaker _bm;
	private ObjectManager _om;
	
	void start ()
	{
		_bm = GetComponent<BuildingMaker>();
		_om = ObjectManager.Instance;
		if(EditorApplication.isPlaying)
			ImportChunkData();
	}
	string _chunkDirectory = "/Models/RuinedBuildingChunks/Resources/";
	
	private void GenerateChunkData() {
		
		string[] props = Directory.GetFiles(Application.dataPath + _chunkDirectory, "*.fbx", SearchOption.AllDirectories);
		File.WriteAllText(Application.dataPath + _chunkDirectory + "BuildingChunkData.json", ProcessDirectory("RuinedBuildingChunks", _chunkDirectory, props));

		AssetDatabase.Refresh();
	}

	private static string ProcessDirectory(string rootName, string rootDir, string[] files) {
		IslandPropItem rootChunkDefinition = new IslandPropItem{ name = rootName };
		foreach (string file in files) {
			string fileRelative = file.Substring(file.LastIndexOf(rootDir, System.StringComparison.Ordinal) + rootDir.Length);
			// cut the file extension off the name.
			fileRelative = fileRelative.Substring(0, fileRelative.LastIndexOf('.'));
			rootChunkDefinition.AddItem(fileRelative);
		}
		return JsonConvert.SerializeObject(rootChunkDefinition);
	}
	
	private void ImportChunkData ()
	{
		IslandPropItem data = JsonConvert.DeserializeObject<IslandPropItem>(File.ReadAllText(Application.dataPath + _chunkDirectory + "BuildingChunkData.json"));
		
		foreach(var building in data.folders)
		{
			if(building.name == _bm.BuildingCulture)
			{
				_bm.BuildingChunkDefinitions = new List<BuildingMaker.ChunkDefinition>();
				for(int i = 0; i < building.folders.Count; i++)
				{
					var tempbm = new BuildingMaker.ChunkDefinition();
					tempbm.ChunkType = building.folders[i].name;
					tempbm.ChunkGo = new List<GameObject>();
					foreach(string f in building.folders[i].files)
					{
						Debug.Log(f);
						tempbm.ChunkGo.Add(_om.GetAssetFromName(f));
					}
					_bm.BuildingChunkDefinitions.Add(tempbm);
				}
			}
			else
			{
				Debug.LogError("No building data for current named building type avaliable!");
			}
		}
	}
}
