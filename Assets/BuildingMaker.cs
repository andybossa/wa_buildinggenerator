﻿using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
using System.IO;
using System.Collections.Generic;

public class BuildingMaker : MonoBehaviour {
	[System.SerializableAttribute]
	public struct ChunkDefinition
	{
		public string ChunkType;
		public Vector3 ReflectionRotation;
		public List<GameObject> ChunkGo;
	}

	public static BuildingMaker Instance { get; private set; }
	
	public string BuildingCulture;
	public string BuildingName;
	public int[] Floors;
	[HeaderAttribute("Chunks avaliable in current plan")]
	public List<ChunkDefinition> BuildingChunkDefinitions;
	public BuildingChunk[] CurrentChunksInBuilding;
	
	public string BuildingGeneratorPath = "/RuinedBuildings/";
	
	public bool Dirty
	{
		get { return _dirty; }
		set { _dirty = value; }
	}
	public int CurrentFloor
	{
		get { return _currentFloor; }
	}
	public BuildingChunk SelectedChunk {get; private set;}
	[SerializeField] private ChunkAssetList _pallet;
	[SerializeField] private Color _selectedChunkColor;
	[SerializeField] private float _startingChunkAlpha = 0.5f;
	[SerializeField] private float _occupiledChunkAlpha = 0.25f;
	public BuildingTool CurrentTool { get; set; }
	
	private BuildingMakerUI _bmui;
	private string _rootPath;
 	private BuildingData _bd = new BuildingData();
	private bool _dirty;
	private int[] _floorCount;
	private BuildingFloor[] _floorData;
	private int _currentFloor = -1;
	private Color _previousChunkSlotColor;
	private BuildingTool _lastTool;
	private UnityEngine.EventSystems.EventSystem ct;
	void Awake ()
	{
		Instance = this;
	}
	
	void Start ()
	{
		_bmui = BuildingMakerUI.Instance;
		CurrentChunksInBuilding = GameObject.FindObjectsOfType<BuildingChunk>();
		_rootPath = Application.dataPath;
		CheckFloors();
	}	
	public void CheckFloors()
	{
		_floorData = gameObject.GetComponentsInChildren<BuildingFloor>();
		Floors = new int[64];
		foreach(var fl in _floorData)
		{
				_floorCount[fl.Floor] += 1;
		}
	}
	
	public void SetFloor(int floor = -1)
	{
		_currentFloor = floor < 0 ? -1 : floor;
		foreach(var fl in _floorData)
		{
			if (fl.Floor == _currentFloor || _currentFloor == -1)
			{
				fl.gameObject.SetActive(true);
			}
			else
			{
				fl.gameObject.SetActive(false);
			}
		}
	}
	
	public void IsolateChunkType(string chunkType)
	{
		foreach(var chunk in CurrentChunksInBuilding)
		{
			chunk.gameObject.SetActive(chunk.ChunkType == chunkType || chunkType == "");
		}		
	}
	
	public void ChunkClicked(BuildingChunk chunk)
	{
		ct = UnityEngine.EventSystems.EventSystem.current;
		if(ct.IsPointerOverGameObject())
			return;
			
		switch (CurrentTool)
		{
			case BuildingTool.Brush:
				SetChunk(chunk, _pallet.ActiveChunkPiece);
				break;
			case BuildingTool.Clear:
				SetChunk(chunk, null);
				break;
			case BuildingTool.Select:
				SelectChunkSlot(chunk);
				break;
		}
	}
	public void SelectChunkSlot (BuildingChunk chunk)
	{	
		if(SelectedChunk)
		{
			SelectedChunk.GetComponent<MeshRenderer>().material.color = _previousChunkSlotColor;
		}
		
		if(!chunk)
			return;
			
		SelectedChunk = chunk;
		var mat = chunk.GetComponent<MeshRenderer>().material;
		_previousChunkSlotColor = mat.color;
		mat.color = _selectedChunkColor;
	}
	
	public void SetChunk(BuildingChunk chunk, GameObject chunkGo)
	{
		if (!chunk || !chunkGo)
		{
			Debug.LogError("SetChunk() being passed 'Null' values");
			return;
		}
		
		_dirty = true;
		if (chunk.CurrentChunk)
			Destroy(chunk.CurrentChunk);
		Transform trans = chunk.transform;
		var mat = chunk.GetComponent<MeshRenderer>().material;
		
		if (!chunkGo)
		{
			mat.color = new Color(mat.color.r,mat.color.g,mat.color.b,_startingChunkAlpha);
			return;
		}
		else
		{
			mat.color = new Color(mat.color.r,mat.color.g,mat.color.b,_occupiledChunkAlpha);		
		}
										
		chunk.CurrentChunk = GameObject.Instantiate(chunkGo, trans.position, trans.rotation * Quaternion.identity) as GameObject;
		chunk.CurrentChunk.name = chunkGo.name;
		chunk.CurrentChunk.transform.SetParent(chunk.transform, true);
	}
	
	public GameObject GetSelectedChunkBrush(ChunkAssetSubGroup activeChunkPallet)
	{
		if(activeChunkPallet.chunkList.gameObject.activeSelf)
		{
			foreach(var tgl in activeChunkPallet.chunkList.GetComponentInChildren<ToggleGroup>().ActiveToggles())
			{
				return tgl.GetComponent<AssetButton>().asset;
			}
		}
		return null;
	}
	
	public void OnClear()
	{
		BuildingCulture = "";
		foreach(var ch in CurrentChunksInBuilding)
		{
			Destroy(ch.CurrentChunk);
			var mat = ch.GetComponent<MeshRenderer>().material;
			mat.color = new Color(mat.color.r,mat.color.g,mat.color.b,_occupiledChunkAlpha);
			ch.ChunkId = -1;
		}
		_dirty = false;
	}
	
	 public void OnSave() {
        _dirty = false;
        
		string fileName = _rootPath + BuildingGeneratorPath;
		
		if(!Directory.Exists(fileName))
			Directory.CreateDirectory(fileName);

        var file = new StreamWriter(fileName + "/" + _bmui.BuildingNameField.text + ".json");
		BuildingData bd = GetData();
        file.WriteLine(JsonConvert.SerializeObject(bd, Formatting.Indented));
        file.Close();
    }
	
	public void OnLoad(string fileToLoad) {
        try {
			Debug.Log("Loading:" + fileToLoad);
			OnClear();
			string fileName = _rootPath + BuildingGeneratorPath + "/" + fileToLoad + ".json";
            var file = new StreamReader(fileName);
			_bd = JsonConvert.DeserializeObject<BuildingData>(file.ReadToEnd());
            file.Close();
			
			SetData(_bd);
            
        } catch (System.Exception e) {
            Debug.LogException(e);
        }
		_dirty = false;
    }
	
	BuildingData GetData()
	{
		var bd = new BuildingData();
		
		for(int i = 0; i < CurrentChunksInBuilding.Length; i++)
		{
			if (CurrentChunksInBuilding[i].CurrentChunk)
			{
				bd.ChunkSlotId.Add(i); // We'll need a much more roubust way of ensuring this order remains the same
				bd.ChunkName.Add(CurrentChunksInBuilding[i].CurrentChunk.name);
			}
		}
		return bd; 
	} 
	
	void SetData(BuildingData bd)
	{
		Debug.Log("Setting Data with " + bd.ChunkSlotId.Count + " Chunks");
		for(int i = 0; i < bd.ChunkSlotId.Count; i++)
		{
			SetChunk(CurrentChunksInBuilding[bd.ChunkSlotId[i]], ObjectManager.Instance.GetAssetFromName(bd.ChunkName[i]));
		}
	}  
	
	void Update ()
	{
		if (CurrentTool != BuildingTool.Select)
		{
			SelectChunkSlot(null);
		}
		if (CurrentTool != BuildingTool.Brush)
		{
			IsolateChunkType("");
		}
	}
}