using UnityEngine;
using UnityEngine.UI;

public class GenericSendMessage : MonoBehaviour {
	
	private Toggle _tgl;
	private bool _previousState;
	void Awake ()
	{
		_tgl = GetComponent<Toggle>();
	}
	void Update () 
	{
		if(_tgl.isOn && !_previousState)
			_tgl.group.SendMessageUpwards("UpdateLevelLoadSelected");
			_previousState = _tgl.isOn;	
	}
}