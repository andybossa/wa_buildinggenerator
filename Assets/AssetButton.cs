﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AssetButton : MonoBehaviour {

	public RawImage texture;
	public Toggle toggle;
	public GameObject asset;
	public Text height;
}