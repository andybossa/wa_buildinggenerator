using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChunkAssetSubGroup
{
	public string chunkType;
	public Toggle chunkButton;
	public ScrollRect chunkList;
	public List<AssetButton> chunkAssetButtons;
	
	public ChunkAssetSubGroup (string ct, Toggle cb, ScrollRect sr)
	{
		chunkType = ct;
		chunkButton = cb;
		chunkList = sr;
		chunkAssetButtons = new List<AssetButton>();
	}
} 
public class ChunkAssetList : MonoBehaviour {
	
	[SerializeField] private AssetButton _assetButtonPrefab;
	[SerializeField] private GameObject _refScrollObject;
	[SerializeField] private GameObject _narrowButton;
	[SerializeField] private Transform _chunkListScrollArea;
	[SerializeField] private Vector2 _listPosition;
	[SerializeField] private Vector2 _listSize;
	public int layerToUseForPreviews;
	private Camera _cam;
	private string _activeChunkType;
	private GameObject _activeChunkPiece;
	private RenderTexture _rt;
	private BuildingMaker _bm;
	private BuildingMakerUI _bmui;
	private List<ChunkAssetSubGroup> _chunkListUI;
	public List<ChunkAssetSubGroup> ChunkListUI { 
		get { return _chunkListUI; } }
	public string ActiveChunkType
	{
		get { return _activeChunkType; }
	}
	public GameObject ActiveChunkPiece
	{
		get { return _activeChunkPiece; }
	}

	protected void Start() {
		_bm = BuildingMaker.Instance;
		_bmui = BuildingMakerUI.Instance;
		_cam = new GameObject("CamForAssets", typeof(Camera)).GetComponent<Camera>();
		_cam.backgroundColor = Color.clear;
		_cam.clearFlags = CameraClearFlags.Color;
		_cam.cullingMask = 1 << layerToUseForPreviews;
		_cam.transform.position = Vector3.zero;
		_cam.renderingPath = RenderingPath.Forward;

		_rt = new RenderTexture(128, 128, 24, RenderTextureFormat.ARGB32);
		
		Light l = new GameObject("Light", typeof(Light)).GetComponent<Light>();
		l.type = LightType.Directional;
		l.intensity = 0.06f;
		l.transform.parent = _cam.transform;
		l.transform.localPosition = Vector3.zero;
		l.transform.localRotation = Quaternion.Euler(0, 45, 0);
		
		_chunkListUI = new List<ChunkAssetSubGroup>();
		
		for (int ct = 0; ct < _bm.BuildingChunkDefinitions.Count; ct++)
		{
			// Gemerate container scroll area for each asset list
			GameObject listScrollArea = GameObject.Instantiate(_refScrollObject, Vector3.zero, Quaternion.identity) as GameObject;
			listScrollArea.name = _bm.BuildingChunkDefinitions[ct].ChunkType.ToString();
			listScrollArea.transform.SetParent(transform,false);
			var rect = listScrollArea.GetComponent<RectTransform>();
			rect.pivot = Vector2.zero;
			rect.anchorMax = new Vector2(0f,gameObject.GetComponent<RectTransform>().anchorMax.y);
			rect.anchorMin = new Vector2(0f,gameObject.GetComponent<RectTransform>().anchorMin.y);
			Transform container = listScrollArea.GetComponent<ScrollRect>().content;
			var listTglGroup = container.gameObject.AddComponent<ToggleGroup>();
			
			// Add toggle button to navigate between the above lists
			var btn = GameObject.Instantiate(_narrowButton, Vector3.zero, Quaternion.identity) as GameObject;
			btn.name = _bm.BuildingChunkDefinitions[ct].ChunkType.ToString();
			btn.GetComponentInChildren<Text>().text = _bm.BuildingChunkDefinitions[ct].ChunkType.ToString();
			btn.transform.SetParent(_chunkListScrollArea, false);
			btn.GetComponent<Toggle>().group = _chunkListScrollArea.GetComponent<ToggleGroup>();
			
			// Create reference to new group for access later
			_chunkListUI.Add(new ChunkAssetSubGroup(_bm.BuildingChunkDefinitions[ct].ChunkType, btn.GetComponent<Toggle>(),listScrollArea.GetComponent<ScrollRect>()));
			
			// Generate screenshots of each asst and add them to previously created list
			for (int i = 0; i < _bm.BuildingChunkDefinitions[ct].ChunkGo.Count; i++) 
			{
				GameObject previewAsset = (GameObject)Instantiate(_bm.BuildingChunkDefinitions[ct].ChunkGo[i], Vector3.zero, Quaternion.identity);
				previewAsset.transform.SetLayerRecursively(layerToUseForPreviews);
				
				Bounds combinedBounds = new Bounds();
				foreach (Renderer render in previewAsset.GetComponentsInChildren<Renderer>()) 
				{
					combinedBounds.Encapsulate(render.bounds);
				}
	
				float width = Mathf.Max(combinedBounds.size.x, combinedBounds.size.z);
				float height = combinedBounds.size.y;
	
				float frustumHeight = Mathf.Max(width, height);
				//target.localScale = new Vector3(1/frustumHeight, 1/frustumHeight, 1/frustumHeight);
	
				float distance = frustumHeight * 0.5f / Mathf.Tan(_cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
	
				_cam.transform.position = new Vector3(0, distance, -distance) ;
				_cam.transform.LookAt(combinedBounds.center);
				
				_rt.DiscardContents();
				
				RenderTexture.active = _rt;
				_cam.targetTexture = _rt;
	
				_cam.Render();
			
				Texture2D preview = new Texture2D(128, 128, TextureFormat.ARGB32, false);
				preview.ReadPixels(new Rect(0, 0, 128, 128), 0, 0);
				preview.Apply();
				DestroyImmediate(previewAsset);
	
				AssetButton button = (AssetButton)Instantiate(_assetButtonPrefab);
				button.toggle.group = listTglGroup;
				button.height.text = height.ToString("0.0") + "m";
				button.texture.texture = preview;
				button.transform.SetParent(container.transform,false);
				button.asset = _bm.BuildingChunkDefinitions[ct].ChunkGo[i];
	
				_chunkListUI[ct].chunkAssetButtons.Add(button);
			}
			if(ct != 0)
			{
				listScrollArea.SetActive(false);
			} 
			else
			{
				btn.GetComponent<Toggle>().isOn = true;
			}
		
			var scroll = listScrollArea.GetComponentsInChildren<Scrollbar>();
				foreach(var s in scroll)
				{
					s.value = 1f;
				}
		}

		_cam.targetTexture = null;
		RenderTexture.active = null;
		Destroy(_cam.gameObject);
		Destroy(_refScrollObject);
		Destroy(l);
		
			
	}
	
	void Update ()
	{
		foreach(var tgl in _chunkListUI)
		{
			if(tgl.chunkButton.isOn)
			{
				tgl.chunkList.gameObject.SetActive(true);
				if(tgl.chunkType != _activeChunkType && _bm.CurrentTool == BuildingTool.Brush)
				{
					_bm.IsolateChunkType(tgl.chunkType);
					_activeChunkType = tgl.chunkType;
				}
			}
			else
			{
				tgl.chunkList.gameObject.SetActive(false);
			}
		}	
	}
	
	public void UpdateSeledctedBrush()
	{
		foreach(var tgl in _chunkListUI)
		{
			if(tgl.chunkButton.isOn)
			{
				_activeChunkPiece = _bm.GetSelectedChunkBrush(tgl);
				if(_bm.CurrentTool == BuildingTool.Select)
					_bm.SetChunk(_bm.SelectedChunk, _activeChunkPiece);
			}
		}
	}
}