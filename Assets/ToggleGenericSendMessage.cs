﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleGenericSendMessage : MonoBehaviour {
	
	[SerializeField] private string _messageToSend;
	[SerializeField] private bool _sendMessageUp = false;
	private Toggle _tgl;
	private bool _previousState;
	void Awake ()
	{
		_tgl = GetComponent<Toggle>();
	}
	void Update () 
	{
		if(_tgl.isOn && !_previousState)
			if(_sendMessageUp)
			{
				_tgl.group.SendMessageUpwards(_messageToSend);	
			}
			else
			{
				_tgl.group.SendMessage(_messageToSend);
			}
			
			_previousState = _tgl.isOn;	
	}
}