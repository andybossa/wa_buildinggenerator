using System.Collections.Generic;

public class BuildingData
{
	public List<int> ChunkSlotId = new List<int>();
	public List<string> ChunkName = new List<string>();
}